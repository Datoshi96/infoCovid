import React from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import "./index.css";
import Home from './views/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {
//   BrowserRouter as Router,
//   Routes,
//   Route,
//   Link,
// } from "react-router-dom";

// import Home from "./views/Home";
// import Login from "./views/Login";
// import Register from "./views/Register";
// import About from "./views/About";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Home/>
    </div>
    // <Router>
    //   <nav>
    //     <Link to="/">Home</Link>
    //     <Link to="/about">About</Link>
    //     <Link to="/login">Login</Link>
    //     <Link to="/register">Register</Link>
    //   </nav>

    //   <Routes>
    //       <Route path="/" element={Home} />
    //       <Route path="/about" element={About} />
    //       <Route path="/login" element={Login} />
    //       <Route path="/register" element={Register} />
    //   </Routes>
    // </Router>
  );
}

export default App;
