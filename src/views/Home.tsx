import { useEffect, useState } from "react"
import Table from 'react-bootstrap/Table';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye,faTrash } from '@fortawesome/free-solid-svg-icons'
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Services from "../services/Services";
import Form from 'react-bootstrap/Form';
import "react-datepicker/dist/react-datepicker.css";
import { Formik } from "formik";
import * as yup from 'yup';
import Modal from 'react-bootstrap/Modal';

const Home = () => {
  //Interfaces para tipar los valores a usar

  //Interface para la respuesta del servicio que trae el total confirmados, total fallecidos y total recuperados
  interface TotalWorld {
    TotalConfirmed: number;
    TotalDeaths: number;
    TotalRecovered:number,
  }
  //Interface para el manejo de variables internas para el modal
  interface TotalCountry {
    country:string,
    tconf: number;
    tdeath: number;
    treco:number,
  }
  //Interface para la respuesta del servicio que trae la información por país
  interface CountryData {
    ID: string,
    Country: string,
    CountryCode: string,
    Province: string,
    City: string,
    CityCode: string,
    Lat: string,
    Lon: string,
    Confirmed: number,
    Deaths: number,
    Recovered: number,
    Active: number,
    Date: string,
  }
  //Interface para la respuesta del servicio que trae la información de todos los paises
  interface TotalCountries {
    ID: string,
    Country: string,
    CountryCode: string,
    Slug: string,
    NewConfirmed: number,
    TotalConfirmed: number,
    NewDeaths: number,
    TotalDeaths: number,
    NewRecovered: number,
    TotalRecovered: number,
    Date: string,
    Premium: object,
  }
  //Interface para los datos recopilados de las personas que pueden tener covid
  interface DataUsers {
    name: string,
    lastname: string,
    date: string,
    temrature: string,
    symptom:{
      fever:boolean,
      cough:boolean,
      pain:boolean,
    },
  }
  // Constantes usadas en el aplicativo
  const [totalWorld, setTotalWorld] = useState<TotalWorld>();
  const [totalCountries, setTotalCountries] = useState<TotalCountries[]>();
  const [filter, setFilter] = useState<string>("");
  const [showTableAll, setShowTableAll] = useState<boolean>(true);
  const [dataUser, setDataUser] = useState<DataUsers[]>()
  const [show, setShow] = useState<boolean>(false);
  const [countryDataT, setCountryDataT] = useState<TotalCountry>()
  const [countryData, setCountryData] = useState<CountryData[]>()

//Esquema para validar los campos que no se diligencien en el formulario
  const schema = yup.object().shape({ name: yup.string().required("Debe ingresar el nombre"), 
                                      lastname: yup.string().required("Debe ingresar el apellido"),
                                      date: yup.string().required("Debe ingresar la fecha"),
                                      temperature:  yup.string().required("Debe ingresar la temperatura"),
                                      });
  //Método que llama al servicio de traer el total global
  const getTotalWorld = () =>{
    Services.getTotalWorld().then(res=>{
      setTotalWorld(res);
    })
    
  }
  //Método que llama el servicio para traer el total de paises
  const getTotalCountries = () =>{
    Services.getTotalCountries().then(res=>{
      setTotalCountries(res)
    })
  }

  //Método para la busqueda por nombre
  const searchText = (e:any) =>{
    setFilter(e.target.value);
  }

  //Método para adicionar un registro a la tabla de personas ingresadas
  const addElement = (values:any) => {
    if(dataUser){
      const array = [...dataUser,values]
      setDataUser(array)
    }else{
      setDataUser([values])
    }
  }
 
  //Método para eliminar un elemento de la tabla de personas ingresadas
  const deleteElement = (index:number) =>{
    const array = dataUser?.filter((_,i)=> i !== index)
    setDataUser(array)
  }

  //Método para mostrar el detalle de cada país con el llamado del servicio que trae información por país.
  //NOTA: Se pueden traer los ultimos 360 con este metodo pero preferi mostrar los ultimos 60 para no ver tantos registros.
  const details = (country:string,tconf:number,tdeath:number,treco:number) => {
    setCountryDataT({country:country,tconf:tconf,tdeath:tdeath,treco:treco})
    Services.getCountry(country).then(res=>{
      const array = res.slice(res.length-60)
      setCountryData(array);
    })
    setShow(true)
  }

  useEffect(() => {
    getTotalWorld();
    getTotalCountries();
},[])
  //Componentes para mostrar la pantalla
  return (
  <div>
    <Container className="mt-10">
      <h4 className="mt-3 mb-3">Bienvenido a la app de información de COVID - creado por [David - 3125012258]</h4>
    </Container>
    <Container>
      <Row className="justify-content-md-center">
        <Col md="auto">
        <Card border="dark" style={{ width: '18rem' }}>
          <Card.Body>
            <Card.Title>Confirmados</Card.Title>
            <Card.Text>
              {totalWorld?.TotalConfirmed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
            </Card.Text>
          </Card.Body>
        </Card>
        </Col>
        <Col md="auto">
        <Card border="dark" style={{ width: '18rem' }}>
          <Card.Body>
            <Card.Title>Fallecidos</Card.Title>
            <Card.Text>
              {totalWorld?.TotalDeaths.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
            </Card.Text>
          </Card.Body>
        </Card>
        </Col>
        <Col md="auto">
        <Card border="dark" style={{ width: '18rem',marginBottom:'10px' }}>
          <Card.Body>
            <Card.Title>Recuperados</Card.Title>
            <Card.Text>
              {totalWorld?.TotalRecovered.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
            </Card.Text>
          </Card.Body>
        </Card>
        </Col>
      </Row>
    </Container>
    {
      showTableAll ? 

        <Container>
          <Container>
            <Button variant="primary" onClick={()=>setShowTableAll(!showTableAll)}>Ingresar personas</Button>
          </Container>
          <div className="input-group mb-3 mt-5">
              <input type="text" onChange={(text) => searchText(text)} className="form-control" 
              placeholder="Buscar por nombre" aria-describedby="button-addon2"/>
          </div>
          <div className="table-responsive">
            <Table striped bordered hover>
                <thead>
                    <tr className="font-luckies">
                        <th>Nombre país</th>
                        <th>Total casos confirmados</th>
                        <th>Total fallecidos</th>
                        <th>Total recuperados</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                {totalCountries &&
                      totalCountries.filter((country) => {
                          if (filter === ""){
                              return country
                          }else if(country.Country.toLowerCase().includes(filter.toLowerCase())){
                              return country
                          }
                      }).map((country) => {
                          return <tr key={country.ID}>
                                    <td>{country.Country}</td>
                                    <td>{country.TotalConfirmed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>
                                    <td>{country.TotalDeaths.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>
                                    <td>{country.TotalRecovered.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>
                                    <td>
                                    <Button onClick={()=>details(country.Country,country.TotalConfirmed,country.TotalDeaths,country.TotalRecovered)} variant="dark"><FontAwesomeIcon color="#fff" icon={faEye} /></Button>
                                    </td>
                                </tr>
                      }
                      )

                      }
                </tbody>
            </Table>
          </div>
        </Container>

      :
      <Container>
        <Container>
          <Button variant="primary" onClick={()=>setShowTableAll(!showTableAll)}>Regresar</Button>
        </Container>
        <Container fluid>
          <Row className="gap-3">
            <Col xs={12} sm={12} lg={12} xl={12}>
              <Card className="mt-5">
                <Card.Body>
                  <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                      addElement(values);
                    }}
                    initialValues={{
                      name: "",
                      lastname: "",
                      date: "",
                      temperature: "",
                      symptom:{
                        fever:false,
                        cough:false,
                        pain:false
                      },
                    }}
                  >
                  {({ handleSubmit, handleChange, values, isValid, errors, dirty }) => (
                    <Form noValidate onSubmit={handleSubmit}>
                      <Row>
                        <Col xs={12} sm={12} lg={4} xl={4}>
                          <Form.Label>Nombres</Form.Label>
                          <Form.Control 
                          name="name"
                          value={values.name}
                          onChange={handleChange}
                          isInvalid={!!errors.name} />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.name}
                          </Form.Control.Feedback>
                        </Col>
                        <Col xs={12} sm={12} lg={4} xl={4}>
                          <Form.Label>Apellidos</Form.Label>
                          <Form.Control
                            name="lastname"
                            value={values.lastname}
                            onChange={handleChange}
                            isInvalid={!!errors.lastname}
                          />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.lastname}
                          </Form.Control.Feedback>
                        </Col>
                        <Col xs={12} sm={12} lg={4} xl={4}>
                          <Form.Label>Fecha de nacimiento</Form.Label>
                          <Form.Control
                            type="date"
                            name="date"
                            value={values.date}
                            onChange={handleChange}
                            isInvalid={!!errors.date}
                          />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.date}
                          </Form.Control.Feedback>
                        </Col>
                        <Col xs={12} sm={12} lg={4} xl={4} className="mt-2">
                          <Form.Label>Temperatura</Form.Label>
                          <Form.Control 
                            name="temperature"
                            value={values.temperature}
                            onChange={handleChange}
                            isInvalid={!!errors.temperature}
                          />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.temperature}
                          </Form.Control.Feedback>
                        </Col>
                        <Col xs={12} sm={12} lg={4} xl={4} className="mt-2">
                          <Form.Label>Sintomas</Form.Label>
                          <Row>
                            <Col>
                              <Form.Check
                                type={'checkbox'}
                                label='Fiebre'
                                name="symptom.fever"
                                onChange={handleChange}
                              />
                            </Col>
                            <Col>
                              <Form.Check
                                type={'checkbox'}
                                label='Tos'
                                name="symptom.cough"
                                onChange={handleChange}
                              />
                            </Col>
                            <Col>
                              <Form.Check
                                type={'checkbox'}
                                label='Dolor'
                                name="symptom.pain"
                                onChange={handleChange}
                              />
                            </Col>
                          </Row>
                          
                        </Col>
                        <Col xs={12} sm={12} lg={12} xl={12} className="mt-2 text-center">
                          <Button disabled={!(isValid && dirty)} type="submit">Enviar</Button>
                        </Col>
                      </Row>

                    </Form>
                    )  
                  }
                    
                  </Formik>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          {
            dataUser &&
              <div className="table-responsive mt-3">
                <Table striped bordered hover>
                    <thead>
                        <tr className="font-luckies">
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Fecha nacimiento</th>
                            <th>Sintomas</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    {dataUser.length > 0 &&
                          dataUser.map((data,index) => {
                              return <tr key={index}>
                                        <td>{data.name}</td>
                                        <td>{data.lastname}</td>
                                        <td>{data.date}</td>
                                        <td>{data.symptom.fever&&'Fibre'}{data.symptom.cough&&' Tos'}{data.symptom.pain&&' Dolor'}</td>
                                        <td>
                                        <Button variant="dark" onClick={()=>deleteElement(index)}><FontAwesomeIcon color="#fff" icon={faTrash} /></Button>
                                        </td>
                                    </tr>
                          }
                          )

                          }
                    </tbody>
                </Table>
              </div>
          }
        </Container>
      </Container>
      
    }
    <Modal
        show={show}
        size="lg"
        onHide={() => setShow(false)}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            {countryDataT?.country}:
          </Modal.Title>
          <Row style={{marginLeft:'5px'}}>
            <Col>
            <Card border="dark" >
              <Card.Body>
                <Card.Title>Confirmados</Card.Title>
                <Card.Text>
                  {countryDataT?.tconf.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                </Card.Text>
              </Card.Body>
            </Card>
            </Col>
            <Col>
            <Card border="dark">
              <Card.Body>
                <Card.Title>Fallecidos</Card.Title>
                <Card.Text>
                  {countryDataT?.tdeath.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                </Card.Text>
              </Card.Body>
            </Card>
            </Col>
            <Col>
            <Card border="dark" style={{ marginBottom:'10px' }}>
              <Card.Body>
                <Card.Title>Recuperados</Card.Title>
                <Card.Text>
                  {countryDataT?.treco.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                </Card.Text>
              </Card.Body>
            </Card>
            </Col>
          </Row>
        </Modal.Header>
        <Modal.Body>
        {
            countryData &&
              <div className="table-responsive mt-3">
                <Table striped bordered hover>
                    <thead>
                        <tr className="font-luckies">
                            <th>Confirmados</th>
                            <th>Fallecidos</th>
                            <th>Recuperados</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                    {countryData.length > 0 &&
                          countryData.map((data,index) => {
                              return <tr key={index}>
                                        <td>{data.Confirmed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>
                                        <td>{data.Deaths.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>
                                        <td>{data.Recovered.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>
                                        <td>{new Date(data.Date).toLocaleDateString()}</td>
                                    </tr>
                          }
                          )

                          }
                    </tbody>
                </Table>
              </div>
          }
        </Modal.Body>
      </Modal>
  </div>
  )
};

export default Home;