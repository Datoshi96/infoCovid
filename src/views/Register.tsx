import * as React from "react";
import {
    withLastLocation,
  WithLastLocationProps
} from "react-router-last-location";

const Register: React.FC<WithLastLocationProps> = ({ lastLocation }) => {

  return (
    <div>
      <h1>Register!</h1>
      <p>
        This component is using new React's feature -{" "}
        <a href="https://reactjs.org/docs/hooks-overview.html">register</a>.
      </p>
    </div>
  );
};

export default withLastLocation(Register);