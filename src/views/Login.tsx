import * as React from "react";
import {
  withLastLocation,
  WithLastLocationProps
} from "react-router-last-location";

const Login: React.FC<WithLastLocationProps> = ({ lastLocation }) => {

  return (
    <div>
      <h1>Login!</h1>
      <p>
        This component is using new React's feature -{" "}
        <a href="https://reactjs.org/docs/hooks-overview.html">login</a>.
      </p>
      <h2>Your last location</h2>
    </div>
  );
};

export default withLastLocation(Login);