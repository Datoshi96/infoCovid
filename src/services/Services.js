import axios from 'axios';


export default {
    getTotalWorld : () => {
        const response = axios.get(`https://api.covid19api.com/world/total`).then( res =>{
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },
    getTotalCountries : () => {
        const response = axios.get(`https://api.covid19api.com/summary`).then( res =>{
            return res.data.Countries;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },

    getCountry : (country) => {
        const response = axios.get(`https://api.covid19api.com/dayone/country/${country}`).then( res =>{
            console.log(res.data)
            return res.data;
        }).catch(error => {
            console.log(error);
            return error;
        })
        return response
    },
}