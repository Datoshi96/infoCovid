const menuItems = [
    {
      title: "Inicio",
      url: "#",
      cName: "nav-links"
    },
    {
      title: "Datos",
      url: "#",
      cName: "nav-links"
    },
    {
      title: "Sobre nosotros",
      url: "#",
      cName: "nav-links"
    },
    {
      title: "Registrarse",
      url: "#",
      cName: "nav-links"
    },
    {
      title: "Iniciar sesión",
      url: "#",
      cName: "nav-links"
    }
  ];
  
  export default menuItems;
  