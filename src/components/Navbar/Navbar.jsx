import React, { useState } from "react";
import menuItems from "../MenuItems";
import "./Navbar.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes,faBars,faVirusCovid } from '@fortawesome/free-solid-svg-icons'

const Navbar = () => {
  const [active, setActive] = useState(false);

  const handleClick = () => {
    setActive(!active);
  };

  return (
    <nav className="navbar">
      <h1 className="navbar-logo">
        InfoCovid <FontAwesomeIcon icon={faVirusCovid} />
      </h1>
      <div className="menu-icon" onClick={handleClick}>
        <FontAwesomeIcon color="#fff" icon={active? faTimes:faBars} />
      </div>
      <ul className={active ? "nav-menu active" : "nav-menu"}>
        {menuItems.map((item, index) => {
          return (
            <li key={index}>
              <a href={item.url} className={item.cName}>
                {item.title}
              </a>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default Navbar;